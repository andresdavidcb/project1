<form method="POST" v-on:submit.prevent="updateQuestion(fillquest.id)">
<div class="modal fade" id="edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
				<h4>Editar pregunta</h4>
			</div>
			<div class="modal-body">
				<form>
					<label for="question">Pregunta</label><textarea name="question" v-model="fillquest.question" class="form-control">@{{fillquest.question}}</textarea>	
					<label for="a">Opción A</label><textarea name="a" v-model="fillquest.a" class="form-control">@{{fillquest.a}}</textarea>	
					<label for="b">Opción B</label><textarea name="b" v-model="fillquest.b" class="form-control">@{{fillquest.b}}</textarea>	
					<label for="c">Opción C</label><textarea name="c" v-model="fillquest.c" class="form-control">@{{fillquest.c}}</textarea>	
					<label for="d">Opción D</label><textarea name="d" v-model="fillquest.d" class="form-control">@{{fillquest.d}}</textarea>
					<label for="right_ans">Respuesta correcta</label><br>
					<input type="radio" v-model="fillquest.right_ans" value="a">A &nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="radio" v-model="fillquest.right_ans" value="b">B &nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="radio" v-model="fillquest.right_ans" value="c">C &nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="radio" v-model="fillquest.right_ans" value="d">D		
					<span v-model="error in errors" class="danger">@{{ error }}</span><!--prueba-->

				</form>
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-primary" value="Guardar">
			</div>
			
		</div>
		
	</div>
	
</div>
</form>