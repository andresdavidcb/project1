
@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">LISTA DE PREGUNTAS</div>

                <div class="panel-body" id="main">
                    <div class="col-sm-12">
                        <a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#create" style="margin:5px"><i class=" glyphicon glyphicon-plus"></i> </a>
                        <a href="#" class="btn btn-primary pull-right" v-on:click="getQuestions" style="margin:5px">Revolver</a>
                        <table class="table table-hover table-striped" id='myTable'>
                            <thead>
                                <tr>
                                    <th>Nº</th>
                                    <th>PREGUNTA</th>
                                    <th>A</th>
                                    <th>B</th>
                                    <th>C</th>
                                    <th>D</th>
                                    <th colspan="2">ACCIONES</th>
                            </thead>
                            <tbody>
                                
                                

                                <tr v-for="(item,index) in quest">
                                    <td>@{{ index+1 }}</td>
                                    <td>@{{ item.question }}</td>
                                    <td>@{{ item.a }}</td>
                                    <td>@{{ item.b }}</td>
                                    <td>@{{ item.c }}</td>
                                    <td>@{{ item.d }}</td>
                                    <td width="10px"><a href="#" class="btn-sm btn-primary" v-on:click.prevent="editQuestion(item)"><i class="glyphicon glyphicon-edit"></i></a></td>
                                    <td width="10px"><a href="#" class="btn-sm btn-danger" v-on:click.prevent="delQuestion(item)"><i class="glyphicon glyphicon-remove-sign"></i></a></td>
                                </tr>
                            </tbody>
                            
                        </table>
                        @include('questions.create')
                        @include('questions.edit')
                    </div>
                    <!--<div class="col-md-12">
                        <pre>
                          @{{$data}} 
                        </pre>
                    </div>-->

                    

                </div>
            </div>
        </div>
    </div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js" ></script>
<script src="{{asset('js/app.js')}}"></script>

@endsection
