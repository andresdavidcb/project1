<form method="POST" v-on:submit.prevent="createQuestion">
<div class="modal fade" id="create">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
				<h4>Nueva pregunta</h4>
			</div>
			<div class="modal-body">
				<form>
					<label for="question">Pregunta</label><textarea name="question" v-model="newquest" class="form-control">@{{newquest}}</textarea>	
					<label for="a">Opción A</label><textarea name="a" v-model="A" class="form-control">@{{A}}</textarea>	
					<label for="b">Opción B</label><textarea name="b" v-model="B" class="form-control">@{{B}}</textarea>	
					<label for="c">Opción C</label><textarea name="c" v-model="C" class="form-control">@{{C}}</textarea>	
					<label for="d">Opción D</label><textarea name="d" v-model="D" class="form-control">@{{D}}</textarea>
					<label for="right_ans">Respuesta correcta</label><br>
					<input type="radio" v-model="right_ans" value="a">A &nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="radio" v-model="right_ans" value="b">B &nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="radio" v-model="right_ans" value="c">C &nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="radio" v-model="right_ans" value="d">D	
					<span v-model="error in errors" class="danger">@{{ error }}</span>
				</form>
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-primary" value="Guardar">
			</div>
			
		</div>
		
	</div>
	
</div>
</form>