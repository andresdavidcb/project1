
@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default" id="main">
            	
                <div class="panel-heading">Pregunta
                	<a href="#" class="btn btn-primary pull-right" v-on:click="getQuestion" style="margin-top:-5px">Revolver</a></div>
				
                <div class="panel-body">
                    <div class="col-sm-12">
                        
                        
                        
						<div v-for="(item,index) in quest">
						<p><strong>@{{index+1}}. &nbsp;&nbsp;&nbsp;@{{item.question}}</strong></p>
						A. <input type="radio" v-model="myanswer" value="a"> @{{item.a}}<br>
						B. <input type="radio" v-model="myanswer" value="b"> @{{item.b}}<br>
						C. <input type="radio" v-model="myanswer" value="c"> @{{item.c}}<br>
						D. <input type="radio" v-model="myanswer" value="d"> @{{item.d}}<br><br>
						
						
                        
                    </div>
                </div>
                <!--<div class="panel-footer">
                	<a href="#" class="btn btn-primary" value="Responder" v-on:click.prevent="checkQuestion(item)">Responder</a>
                	
                </div>-->
            	
                	
            </div>
        </div>
    </div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js" ></script>
<script src="{{asset('js/game.js')}}"></script>
@endsection