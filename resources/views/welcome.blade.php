@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body" id="crud">
                    <div class="col-sm-7">
                        <a href="#" class="btn btn-primary pull-right">Nueva pregunta</a>
                        <table class="table table-hover table-sprite">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>PREGUNTA</th>
                                    <th colspan="2"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="item in quest">
                                    <td width="10px">@{{ item.question }}</td>
                                    <td>¿Cuál es el resultado de 1 + 4?</td>
                                    <td width="10px"><a href="#"><i class="glyphicon glyphicon-edit"></i></a></td>
                                    <td width="10px"><a href="#"><i class="glyphicon glyphicon-remove-sign"></i></a></td>
                                </tr>
                            </tbody>

                            
                        </table>
                    </div>

                    <div class="col-sm-5">
                        <h1>Lista VUE js - JSON</h1>
                        <pre>@{{ $data }}</pre>
                    </div>

                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js" ></script>
<script src="{{asset('js/app.js')}}"></script>
@endsection
