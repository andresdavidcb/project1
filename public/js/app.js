
		new Vue({
			el:'#main',
			created:function(){
				this.getQuestions();

			},
			data:{
				quest:[],
				newquest:'',
				A:'',
				B:'',
				C:'',
				D:'',
				right_ans:'',
				fillquest:{'id':'','question':'','a':'','b':'','c':'','d':'','right_ans':''},
				errors:[],
				i:0

			},
			methods:{
				getQuestions:function(){
					var urlQuestions='questions';
					axios.get(urlQuestions).then(response=>{
						this.quest=response.data.quest.data
					});
				},
				delQuestion:function(item){
					var url='questions/' + item.id;
					axios.delete(url).then(response=>{
						this.getQuestions();
						toastr.success('pregunta eliminada correctamente');
					});
				},
				editQuestion:function(item){
					this.fillquest.id=item.id;
					this.fillquest.question=item.question;
					this.fillquest.a=item.a;
					this.fillquest.b=item.b;
					this.fillquest.c=item.c;
					this.fillquest.d=item.d;
					this.fillquest.right_ans=item.right_ans;
					//alert(item.right_ans);
					$("#edit").modal('show');

				},
				updateQuestion:function(id){

					var url='questions/' + id;
					axios.put(url,this.fillquest).then(response=>{
						this.getQuestions();
						this.fillquest={'id':'','question':'','a':'','b':'','c':'','d':'','right_ans':''};
						this.errors=[];
						$('#edit').modal('hide');
						toastr.success('pregunta guardada correctamente');

					}).catch(error=>{
						this.errors=error.response.data
					});;
				},
				createQuestion:function(){
					var url='questions';
					axios.post(url,{
						question: this.newquest,
						a: this.A,
						b: this.B,
						c: this.C,
						d: this.D,
						right_ans: this.right_ans
					}).then(response=>{
						this.getQuestions();
						this.newquest='';
						this.A='';
						this.B='';
						this.C='';
						this.D='';
						this.right_ans='';
						this.errors=[];
						$('#create').modal('hide');
						toastr.success('Pregunta creada con éxito!')
					}).catch(error=>{
						this.errors=error.response.data
					});
				}
			}
		})