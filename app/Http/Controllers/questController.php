<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\questionRequest;
use App\Models\question;

class questController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $quest=question::where('curso',6)->orderBy('id','asc')->paginate(40);
        //$quest=question::where('curso',6)->inRandomOrder()->paginate(40);
        return [
            'pagination'=>[
                'total'=>$quest->total(),
                'current_page'=>$quest->currentPage(),
                'per_page'=>$quest->perPage(),
                'last_page'=>$quest->lastPage(),
                'from'=>$quest->firstItem(),
                'to'=>$quest->lastPage(),
            ],
            'quest'=>$quest
        ];
        //return view('questions.index');
    }

    public function randomQuestion()
    {
        $quest=question::where('curso',6)->inRandomOrder()->take(1)->get();
        return $quest;
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(questionRequest $request)
    {
        question::create($request->all());
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    //RANDOM QUESTION
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quest=question::findOrFail($id);
        return $quest;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(questionRequest $request, $id)
    {
        question::find($id)->update($request->all());
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quest=question::findOrFail($id);
        $quest->delete();
    }
}
