<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class questionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question'=>'required|min:6',
            /*'a'=>'required',
            'b'=>'required',
            'c'=>'required',
            'd'=>'required',*/
            //'right_ans'=>'required'
        ];
    }
}
